<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<head>
<title>Employee list</title>          
</head>
<body>
 <table style="border-collapse:collapse" border=1;>
<tr>
<th>Name</th>
<th>Adreess</th>
<th>Email</th>
<th>Phone number</th>
</tr>
<c:forEach var="list" items="${Employeelist}">
<tr>
<td>${list.address}</td>
<td>${list.email}</td>
<td>${list.name}</td>
<td>${list.phno}</td>
<td>
<a href="editEmployee?id=${list.id}">Edit</a></td>
<td>
<a href="deleteEmployee?id=${list.id}">Delete</a></td>
</tr>
</c:forEach>
</table>
</body>
</html>