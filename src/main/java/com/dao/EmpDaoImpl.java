package com.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pack.Employee;
@Repository
public class EmpDaoImpl implements EmpDao{
@Autowired
SessionFactory sessionFactory;
	public void addEmployee(Employee e) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(e);
	}
	
	public List<Employee> getAllEmployees() {
		// TODO Auto-generated method stub
       List<Employee> l=sessionFactory.getCurrentSession().createQuery("from Employee").list();
return l;
	}
	public Employee getEmployee(int empid) {
		// TODO Auto-generated method stub
		 Employee e=(Employee) sessionFactory.getCurrentSession().get(Employee.class, empid);
		return e;
	}
	public void updateEmployee(Employee e) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(e);

	}
	public void deleteEmployee(int empid) {
	Employee e=(Employee) sessionFactory.getCurrentSession().get(Employee.class, empid);
    sessionFactory.getCurrentSession().delete(e);
	}                               

}
     