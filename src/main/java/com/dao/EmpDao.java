package com.dao;

import java.util.List;

import com.pack.Employee;

public interface EmpDao {

	

	void addEmployee(Employee e);

	
	List<Employee> getAllEmployees();


	Employee getEmployee(int empid);


	void updateEmployee(Employee e);


	void deleteEmployee(int empid);

}
