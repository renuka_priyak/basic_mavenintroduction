package com.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.EmpDao;
import com.pack.Employee;
@Service
@Transactional
public class EmpServiceImpl implements EmpService {
@Autowired
EmpDao dao;
	public void addEmployee(Employee e) {
		// TODO Auto-generated method stub
		dao.addEmployee(e);
	}
	@Override
	public List<Employee> getAllEmployees() {
		// TODO Auto-generated method stub
		return dao.getAllEmployees();
	}
	@Override
	public Employee getEmployee(int empid) {
		// TODO Auto-generated method stub
		return dao.getEmployee(empid);
	}
	@Override
	public void updateEmployee(Employee e) {
		// TODO Auto-generated method stub
		 dao.updateEmployee(e);
	}
	@Override
	public void deleteEmployee(int empid) {
	       dao.deleteEmployee(empid);
 	}
}
