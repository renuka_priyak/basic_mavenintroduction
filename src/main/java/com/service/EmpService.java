package com.service;

import java.util.List;

import com.pack.Employee;

public interface EmpService {
public void addEmployee(Employee e);

public List<Employee> getAllEmployees();
public Employee getEmployee(int empid);

public void updateEmployee(Employee e);

public void deleteEmployee(int empid);

}
