package com;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.pack.Employee;
import com.service.EmpService;

@Controller
public class EmpController {

	@Autowired
     EmpService empservice;
	@RequestMapping(value="login")
	public ModelAndView getLogin(@ModelAttribute("employee") Employee e)throws IOException{
	return new  ModelAndView("EmployeeForm");
	
}
	@RequestMapping(value="saveEmployee",method=RequestMethod.POST)
	public ModelAndView saveEmployee(@ModelAttribute("employee")@Valid Employee e,BindingResult res)throws IOException{
		
		if(res.hasErrors())
		{
			return new  ModelAndView("EmployeeForm");
		}
		   if(e.getId()==0)
		   {
		empservice.addEmployee(e);
	List<Employee> Employeelist=empservice.getAllEmployees();
	System.out.println(Employeelist);
			return new  ModelAndView("Success","Employeelist",Employeelist);
		   }
		   empservice.updateEmployee(e);
		   List<Employee> Employeelist=empservice.getAllEmployees();
			System.out.println(Employeelist);
					return new  ModelAndView("Success","Employeelist",Employeelist);
		   
}
	@RequestMapping(value="editEmployee",method=RequestMethod.GET)
	public ModelAndView editEmployee(HttpServletRequest req)
	{
		int empid=Integer.parseInt(req.getParameter("id"));
		Employee e=empservice.getEmployee(empid);
		return new  ModelAndView("EmployeeForm","employee",e);
	}
	@RequestMapping(value="deleteEmployee")
	public ModelAndView deleteEmployee(HttpServletRequest req)
	{
		int empid=Integer.parseInt(req.getParameter("id"));
		empservice.deleteEmployee(empid);
		List<Employee> Employeelist=empservice.getAllEmployees();
		return new  ModelAndView("Success","Employeelist",Employeelist);	
	}
}
