package com.pack;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
@Entity
public class Employee {
	@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private int id;
@NotEmpty(message="NAme cannot be empty")
private String name;
@NotEmpty(message="adress cannot be empty")
private String address;
@Email(message="email cannot be empty")
private String email;

private int phno;

public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAddress() {
	return address;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public void setAddress(String address) {
	this.address = address;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public int getPhno() {
	return phno;
}
public void setPhno(int phno) {
	this.phno = phno;
}

}
